#!/bin/bash

##################################################
#Lancement des logiciels et autre pour la démo   #
##################################################
#gnome-terminal \
#  --tab -e "sh start-prometheus-grafana.sh" \
#  --tab -e "sh start-spartiate-master.sh" \
#  --tab -e "sh start-spartiate-non-resilient.sh" \
#  --tab -e "sh start-troyen.sh"

if ! tmux -v COMMAND &> /dev/null
then
    echo "tmux est requis, il va être installé :"
    sudo apt install tmux
fi

tmux new-session -d -s demo

tmux send-keys -t demo 'tmux new-window -n Observabilite ' ENTER
# tmux send-keys -t demo 'tmux new-window -n spartiate ' ENTER
tmux send-keys -t demo 'tmux new-window -n troyen ' ENTER
tmux send-keys -t demo 'tmux new-window -n spartiate-nr ' ENTER


tmux send-keys -t demo "tmux send-keys -t Observabilite 'cd ~/git/orange/spartiate/docker && docker-compose -f spartiate.yml up' ENTER" ENTER

# tmux send-keys -t demo "tmux send-keys -t spartiate 'cd ~/git/orange/spartiate/; git switch -c develop; ./gradlew clean bootRun' ENTER" ENTER
# tmux send-keys -t demo "tmux send-keys -t spartiate 'cd ~/git/orange/spartiate/; ./gradlew clean bootRun' ENTER" ENTER

tmux send-keys -t demo "tmux send-keys -t troyen 'cd ~/git/orange/troyen/; git switch -c troyenV2; ./gradlew clean bootRun' ENTER" ENTER

tmux send-keys -t demo "tmux send-keys -t spartiate-nr 'cd ~/git/spartiate/; git switch -c sans-resilience; ./gradlew clean bootRun' ENTER" ENTER

## Start a new line on window 0
tmux send-keys -t demo ENTER

## Attach to session
tmux send-keys -t demo

tmux attach -t demo
